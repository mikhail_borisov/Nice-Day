//
//  ChartsView.swift
//  Nice Day
//
//  Created by Михаил Борисов on 15.11.2019.
//  Copyright © 2019 Mikhail Borisov. All rights reserved.
//

import Charts
import UIKit

class ChartsView: LineChartView {
    private var lines: [LineChartDataSet] = []

    weak var axisFormatDelegate: AxisValueFormatter?

    init() {
        super.init(frame: .zero)
        axisFormatDelegate = self
        xAxis.labelFont = UIFont.systemFont(ofSize: 10, weight: .semibold)
        scaleXEnabled = false
        scaleYEnabled = false
        rightAxis.enabled = true
        leftAxis.enabled = false
        xAxis.labelTextColor = UIColor.inverseColor.withAlphaComponent(0.6)
        leftAxis.drawGridLinesEnabled = false
        xAxis.avoidFirstLastClippingEnabled = true
        rightAxis.enabled = false
        xAxis.granularityEnabled = true
        xAxis.granularity = 1
        xAxis.drawAxisLineEnabled = false
        pinchZoomEnabled = false
        doubleTapToZoomEnabled = false
        chartDescription.text = ""
        leftAxis.axisMinimum = 0.0
        xAxis.valueFormatter = axisFormatDelegate
        xAxis.forceLabelsEnabled = true
        legend.form = .square
        legend.textColor = .inverseColor
        legend.formSize = 10.0
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    fileprivate func isLegendHidden(_ label: String) {
        if label == "hidden" {
            legend.enabled = false
        }
    }

    func addLine(data: [Double: [Usage]], color: UIColor, label: String) {
        var chartEntry = [ChartDataEntry]()

        let arr = Array(data).sorted { lhs, rhs -> Bool in
            lhs.key < rhs.key
        }

        for (key, value) in arr {
            let total = Double(value.compactMap { $0.total }.reduce(0, +))
            let value = ChartDataEntry(x: key, y: total)
            chartEntry.append(value)
        }
        isLegendHidden(label)
        let line = LineChartDataSet(entries: chartEntry, label: label)
        line.colors = [color]
        line.drawCirclesEnabled = false
        line.drawFilledEnabled = true
        line.fillColor = color
        line.fillAlpha = 1.0
        line.mode = .cubicBezier
        line.drawValuesEnabled = false
        line.highlightEnabled = false
        lines.append(line)
        reloadChart(items: arr.count)
    }

    private func reloadChart(items: Int) {
        let data = LineChartData(dataSets: lines)
        xAxis.labelCount = items
        xAxis.forceLabelsEnabled = true
        notifyDataSetChanged()
        self.data = data
    }
}

extension ChartsView: AxisValueFormatter {
    func stringForValue(_ value: Double, axis _: AxisBase?) -> String {
        let arr = Calendar.current.shortStandaloneWeekdaySymbols.map { $0.capitalized }
        return arr[(Int(value) % 10) % 7]
    }
}
