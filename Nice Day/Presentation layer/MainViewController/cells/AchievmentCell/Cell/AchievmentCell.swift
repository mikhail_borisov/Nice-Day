//
//  AchievmentCell.swift
//  Nice Day
//
//  Created by Михаил Борисов on 16.11.2019.
//  Copyright © 2019 Mikhail Borisov. All rights reserved.
//

import PassKit
import UIKit

class AchievmentCell: UICollectionViewCell {
    static var achievmentIdentifier = "AchievmentCell"

    // MARK: FriendLabel

    fileprivate var achievmentLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 17, weight: .semibold)
        label.text = "Bronze medal"
        label.textAlignment = .left
        return label
    }()

    // MARK: FriendLabel

    fileprivate var achievmentDscrLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 12, weight: .medium)
        label.text = "Earn 100 xp"
        label.textColor = UIColor.black.withAlphaComponent(0.52)
        label.textAlignment = .left
        return label
    }()

    // MARK: AchievmentView

    fileprivate var achievmentView: UIView = {
        let view = UIView()
        view.layer.cornerRadius = 24
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.backgroundColor = UIColor.sunriseColor.cgColor
        return view
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        refresh()
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        refresh()
    }

    private func prepareUI() {
        addSubview(achievmentView)
        addSubview(achievmentLabel)
        addSubview(achievmentDscrLabel)
    }

    private func prepareConstraint() {
        NSLayoutConstraint.activate([
            achievmentView.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            achievmentView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 8),
            achievmentView.widthAnchor.constraint(equalToConstant: 48),
            achievmentView.heightAnchor.constraint(equalToConstant: 48),

            achievmentLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 5),
            achievmentLabel.leadingAnchor.constraint(equalTo: achievmentView.trailingAnchor, constant: 8),

            achievmentDscrLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -5),
            achievmentDscrLabel.leadingAnchor.constraint(equalTo: achievmentLabel.leadingAnchor),
        ])
    }

    private func refresh() {
        prepareUI()
        prepareConstraint()
        let shapeLayer = CAShapeLayer()
        let path = UIBezierPath(arcCenter: .zero, radius: 24, startAngle: -CGFloat.pi / 2, endAngle: 2 * CGFloat.pi, clockwise: true).cgPath
        shapeLayer.path = path
        shapeLayer.position = CGPoint(x: 32, y: 24.5)
        shapeLayer.fillColor = #colorLiteral(red: 0.1960784314, green: 0.8, blue: 0.3882352941, alpha: 1).withAlphaComponent(0.2).cgColor
        contentView.layer.addSublayer(shapeLayer)
        let animation = CABasicAnimation(keyPath: "transform.scale")
        animation.toValue = 1.25
        animation.duration = 0.7
        animation.timingFunction = CAMediaTimingFunction(name: .easeOut)
        animation.repeatCount = Float.infinity
        animation.autoreverses = true
        shapeLayer.add(animation, forKey: nil)
    }
}
